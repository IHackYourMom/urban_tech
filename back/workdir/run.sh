#!/bin/bash

if [[ $1 = "test" ]]; then
	echo "Running test"
	exit 1
fi

if [[ $1 = "migrate" ]]; then

	python manage.py makemigrations
	python manage.py migrate
	exit 1
fi

if [[ $1 = "" ]]; then
	echo "url => 0.0.0.0:21090"
	python manage.py runserver 0.0.0.0:8000
	exit 1
fi
