from rest_framework import serializers
from . models import Wallpaper, Pictures


class WallpaperSerializer(serializers.ModelSerializer):

    class Meta:
        model = Wallpaper
        fields = '__all__'

class PicturesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pictures
        fields = '__all__'
