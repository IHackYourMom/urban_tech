# Generated by Django 2.0.8 on 2018-12-09 16:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20181209_0026'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dialog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip_user', models.CharField(max_length=100, null=True)),
                ('color', models.CharField(max_length=100, null=True)),
                ('size', models.CharField(max_length=100, null=True)),
            ],
        ),
    ]
