import graphene
import graphql_relay
import django_filters
from graphene_django.types import DjangoObjectType
from core.models import Wallpaper
from core.models import Pictures
from graphene_django.filter import DjangoFilterConnectionField

class WallFilter(django_filters.FilterSet):
    class Meta:
        model = Wallpaper
        fields = ['base_material', 'colour', 'water_proof']

class PicFilter(django_filters.FilterSet):
    class Meta:
        model = Pictures
        fields = ['id_market']

class WallNode(DjangoObjectType):
    class Meta:
        model = Wallpaper
        interfaces = (graphene.relay.Node,)

class PicNode(DjangoObjectType):
    class Meta:
        model = Pictures
        interfaces = (graphene.relay.Node,)

class Wallpaper(DjangoObjectType):
    class Meta:
        model = Wallpaper

class Pictures(DjangoObjectType):
    class Meta:
        model = Pictures

class Query(graphene.AbstractType):

    wallpaper = graphene.relay.Node.Field(WallNode)
    wallpapers = DjangoFilterConnectionField(WallNode, filterset_class=WallFilter)

    picture = graphene.relay.Node.Field(PicNode)
    pictures = DjangoFilterConnectionField(PicNode, filterset_class=PicFilter)


    def resolve_all_walpap(self, info, **kwargs):
        return Wallpaper.object.all()

    def resolve_all_pic(self, info, **kwargs):
        return Pictures.objects.select_related('wallpaper').all()

    def resolve_links(self, info, search=None, **kwargs):
        # The value sent with the search parameter will be in the args variable
        if search:
            filter = (
                Wallpaper(url__icontains=search) |
                Pictures(description__icontains=search)
            )
            return Link.objects.filter(filter)

        return Link.objects.all()
