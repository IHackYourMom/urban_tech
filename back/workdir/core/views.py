from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.generics import UpdateAPIView
from rest_framework import generics
from .serializer import WallpaperSerializer, PicturesSerializer
from .models import Wallpaper, Pictures

import json

import nltk

from nltk.corpus import stopwords
from pymystem3 import Mystem
from string import punctuation

import Levenshtein

import random


class Bot():
    def __init__(self):
        nltk.download("stopwords")
        self.mystem = Mystem()
        self.russian_stopwords = stopwords.words("russian")

        self.available_colors = ["бежевый",
                                 "белый",
                                 "бирюзовый",
                                 "голубой",
                                 "желтый",
                                 "зелёно-золотистый",
                                 "зелёный",
                                 "зелёный, золотистый",
                                 "зелёный, серый",
                                 "коричневый",
                                 "красный",
                                 "мультиколор",
                                 "оранжевый",
                                 "розовый",
                                 "серый",
                                 "синий",
                                 "фиолетовый",
                                 "чёрный",
                                ]
        self.available_water_proofs = ["влагостойкие",
                                       "моющиеся",
                                       "обычные",
                                      ]
        self.available_base_material = ["бумага",
                                        "флизелин",
                                       ]
        self.available_words = ["обои","цвет"] + self.available_colors + \
                                                self.available_water_proofs + \
                                                self.available_base_material

        self.color_questions = ["А цвет?",
                                "Какой вы хотите цвет?",
                                "Вы думали над цветом?",
                                "Какой цвет вы предпочитаете?",
                               ]

        self.water_proof_questions = ["Какую влагозащиту предпочитаете?",
                                      "Какую влагозащиту вы хотите?",
                                     ]

        self.base_material_questions = ["Из какого матриала должна быть основа?",
                                        "Какой материал выбрали для основы? Бумага или флизелин?",
                                       ]

        self.greeeting = "Привет! Я твой личный помощник! Я могу помочь выбрать обои."

    def is_value(self, words, available_values):
        for i in range(len(available_values)):
            for word in words:
                if word == available_values[i]:
                    return True, word
        return False, None

    def action_color(self):
        return random.choice(self.color_questions)

    def action_water_proof(self):
        return random.choice(self.water_proof_questions)

    def action_base_material(self):
        return random.choice(self.base_material_questions)

    def action_greet(self):
        return "Привет!"

    def action_bye(self):
        return "Пока"

    def _what_is_word(self, user_word):

        max_stat = 0
        max_word = ''

        for i in range(len(self.available_words)):
            if Levenshtein.ratio(self.available_words[i], user_word) > max_stat:
                max_stat = Levenshtein.ratio(self.available_words[i], user_word)
                max_word = self.available_words[i]

        if max_stat < 0.4:
            max_word = user_word

        return max_word

    def _preprocess_text(self, text):
        tokens = self.mystem.lemmatize(text.lower())

        tokens = [token for token in tokens if token not in self.russian_stopwords \
                  and token != " " \
                  and token.strip() not in punctuation]

        text = " ".join(tokens)

        return text, tokens

    def give_response(self, text, values):

        sentence, tokens = self._preprocess_text(text)

        result = []
        text = ''
        response = {}
        ready = 0

        for token in tokens:
            result.append(self._what_is_word(token))

        values_tmp = {
            "color": None,
            "water_proof": None,
            "base_material": None,
            "ready": False,
        }

        if values["color"] == None:
            res, color = self.is_value(result, self.available_colors)

            if res == True:
                values_tmp["color"] = color
            else:
                text += self.action_color() + ' '

        else:
            values_tmp["color"] = values["color"]

        if values["water_proof"] == None:
            res, water_proof = self.is_value(result, self.available_water_proofs)

            if res == True:
                values_tmp["water_proof"] = water_proof
            else:
                text += self.action_water_proof() + ' '

        else:
            values_tmp["water_proof"] = values["water_proof"]

        if values["base_material"] == None:
            res, base_material = self.is_value(result, self.available_base_material)

            if res == True:
                values_tmp["base_material"] = base_material
            else:
                text += self.action_base_material() + ' '

        else:
            values_tmp["base_material"] = values["base_material"]

        if values_tmp["color"] is not None:
            ready += 1

        if values_tmp["water_proof"] is not None:
            ready += 1

        if values_tmp["base_material"] is not None:
            ready += 1

        if ready == 3:
            values_tmp["ready"] = True
            text = "Вот варианты, которые мы смогли подобрать для вас"

        response["text"] = text
        response["values"] = values_tmp

        return response


class WallpaperList(APIView):

    def get(self, request):
        Wallpaper1 = Wallpaper.objects.all()
        serializer = WallpaperSerializer(Wallpaper1, many=True)
        return Response(serializer.data)


    def post(self, request, format=None):
        print(zaproz)
        zapros = request.data["id_market"]
        objects = Wallpaper.objects.filter(id_market=zapros)
        serializer = WallpaperSerializer(objects, many=True)
        return Response(serializer.data)

class PicturesList(APIView):

    def get(self, request):
        Pictures1 = Pictures.objects.all()
        serializer = PicturesSerializer(Pictures1, many=True)
        return Response(serializer.data)

    def post(self, request):
        print(request.data)
        newPictures = PicturesSerializer(data=request.data)

        if newPictures.is_valid():
            newPictures.save()
        else:
            print(newPictures.errors)

        return Response(newPictures.data)

class WallpaperOne(APIView):

    def get(self, request, pk):
        Wallpaper1 = Wallpaper.objects.get(pk=pk)
        serializer = WallpaperSerializer(Wallpaper1, many=False)
        return Response(serializer.data)


    def post(self):
        pass

class PicturesOne(APIView):

    def get(self, request, pk):
        Pictures1 = Pictures.objects.get(pk=pk)
        serializer = PicturesSerializer(Pictures1, many=False)
        return Response(serializer.data)


    def post(self):
        pass



class BotView(APIView):

    def get(self, request):
        pass

    """
    {
        "text",
        "values" : {
            "color",
            "water_proof",
            "base_material", - основа
        }
    }
    """

    def post(self, request):

        bot = Bot()
        text = request.data["text"]
        values = request.data["values"]
        response_data = bot.give_response(text, values)

        return Response(response_data)
