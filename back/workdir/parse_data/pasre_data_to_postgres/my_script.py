import json

struct = []
pic_path = "slides/"
pk = 5

with open("data_test.json", "r") as file:

    json_data = json.load(file)

    for i in range(3680):
      pk += 1
      template = {
        "model": "core.pictures",
        "pk": None,
        "fields": {
            "id_market": None,
            "image": None,
        }
      }

      id_market = json_data[i]["pk"]
      image = pic_path + json_data[i]["fields"]["id_market"] + ".jpg"

      template["pk"] = pk
      template["fields"]["id_market"] = id_market
      template["fields"]["image"] = image

      struct.append(template)

with open("data_pics.json", "w") as dump:
    json.dump(struct, dump, indent=4)
