import Levenshtein

available_words = ["бежевый",
                   "белый",
                   "бирюзовый",
                   "голубой",
                   "желтый",
                   "зелёно-золотистый",
                   "зелёный",
                   "зелёный, золотистый",
                   "зелёный, серый",
                   "коричневый",
                   "красный",
                   "мультиколор",
                   "оранжевый",
                   "розовый",
                   "серый",
                   "синий",
                   "фиолетовый",
                   "чёрный",
                   "обои",
                   "цвет",
                  ]


def what_is_word(user_word):

    max_stat = 0
    max_word = ''

    for i in range(len(available_words)):
        if Levenshtein.ratio(available_words[i], user_word) > max_stat:
            max_stat = Levenshtein.ratio(available_words[i], user_word)
            max_word = available_words[i]

    print(max_word, max_stat)

    if max_stat < 0.4:
        max_word = user_word

    print(max_word, max_stat)

    return max_word
