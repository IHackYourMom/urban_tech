import nltk
import numpy as np
from gensim.models import Word2Vec
from nltk.tokenize import sent_tokenize, word_tokenize
from gensim import corpora

f = open('colors.txt', 'r', errors = 'ignore')
raw = f.read().lower()

# print(raw)

sent_tokens = sent_tokenize(raw)
word_tokens = word_tokenize(raw)

print(sent_tokens[0])
print(word_tokens[0])

# texts = [[text for text in t.split()] for t in raw]

# dic = corpora.Dictionary(texts)

#model = Word2Vec(data,
#                 size=150,
#                 window=10,
#                 min_count=2,
#                 workers=10)

#model.train(data, total_examples=len(data), epochs=10)

#print(model.most_similar(positive=['цвета']))

# print(dic)
