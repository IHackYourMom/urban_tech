import nltk
import random
import string
import numpy as np

from ru_preprocess import preprocess_text

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

f = open('goods.txt', 'r', errors = 'ignore')
raw = f.read()

raw = raw.lower()

nltk.download('punkt')
nltk.download('wordnet')

sent_tokens, word_tokens = preprocess_text(raw)

# print(preprocess_text(raw))

# sent_tokens = nltk.sent_tokenize(raw)
# word_tokens = nltk.word_tokenize(raw)

# print(sent_tokens)
# print(word_tokens)

lemmer = nltk.stem.WordNetLemmatizer()

def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]

remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

def LemNormalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

def response(user_response):
    robo_response=''

    TfidfVec = TfidfVectorizer(tokenizer=LemNormalize)
    tfidf = TfidfVec.fit_transform(sent_tokens)
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx = vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]

    if(req_tfidf == 0):
        robo_response = robo_response + "Чего, бля?"
        return robo_response
    else:
        robo_response = robo_response + sent_tokens[idx]
        return robo_response

flag = True
print("ЛЕРУЛЬ: Вечер в хату, епта! Меня зовут Леруль, я помогу тебе выбрать товар в твою нору")

while(flag == True):
    user_response = input("ВЫ: ")
    user_response = user_response.lower()

    if(user_response != 'пока'):
        if(user_response == 'спасибо'):
            flag = False
            print('ЛЕРУЛЬ: Да не за что, епт!')
        else:
            sent_tokens.append(user_response)
            word_tokens = word_tokens + nltk.word_tokenize(user_response)
            final_words = list(set(word_tokens))
            print("ЛЕРУЛЬ: ", end="")
            print(response(user_response))
            sent_tokens.remove(user_response)
    else:
        flag = False
        print("ЛЕРУЛЬ: Пиздуй отсюда")
