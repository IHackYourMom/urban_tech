from ru_preprocess import preprocess_text
from nltk.stem.porter import *

from collections import Counter

def stem_tokens(tokens, stemmer):
    stemmed = []
    
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed


pr_tx, tokens = preprocess_text("Ну что сказать, я вижу кто-то наступил на грабли, Ты разочаровал меня, ты был натравлен.")

# stemmer = PorterStemmer()
# stemmed = stem_tokens(tokens, stemmer)
# count = Counter(stemmed)

# print(count.most_common(100))

print(pr_tx)
