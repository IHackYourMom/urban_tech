import React, { Component } from 'react';
import './App.css';
import SpeechAssistance from './components/speechAssistance'


var recognition = new window.webkitSpeechRecognition();
recognition.lang = 'ru-RU';
recognition.interimResults = false;
recognition.maxAlternatives = 1;


// document.body.onclick = function() {
//   recognition.start();
// }

recognition.onresult = function(event) {

  var last = event.results.length - 1;

  console.log(event.results[last][0].transcript);
}

recognition.onspeechend = function() {
  recognition.stop();
}

recognition.onnomatch = function(event) {
  console.log(event.results[0])
}

recognition.onerror = function(event) {
  console.log("Error")
} 


export default class App extends Component {    
    render() {
        return (
          <div>
            <div style={{
            }}>
              <button onClick={ ()=> {
                let ass = document.getElementById('ass')
                ass.style.display = 'block'
              } }>Show</button>
              <SpeechAssistance/>
            </div>            
          </div>
        )
    }
}
