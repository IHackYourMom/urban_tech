import React,{ Component } from 'react'


export default class AssisMsg extends Component {
    render() {
      console.log(this.props)
      // this.props.getState(this.props.text)
      return (
        <div style={{
          borderRadius: '0px 14px 14px 14px',
          width: 150,
          border: "1px solid #777",
          textAlign: 'center',
          margin: 5,
          boxShadow : "0px 0 30px 2px #1A3457",
          background: "#8be2bf"
  
        }}>
          { this.props.text }
        </div>
      )
    }
  }