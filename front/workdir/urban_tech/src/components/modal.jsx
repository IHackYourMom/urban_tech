import React,{ Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import { runInThisContext } from 'vm';
// import { Button } from 'react-materialize'

import { QueryRenderer, graphql } from 'react-relay'

import environment from '../Environment'



const MyQuery = graphql`
query modalQuery($first: Int!, $baseMaterial: String!, $colour: String!, $waterProof: String!){
 wallpapers(first: $first, baseMaterial: $baseMaterial, colour: $colour, waterProof: $waterProof){
  edges{
    node{
      baseMaterial
      colour
      waterProof
      picturesSet{
        image
          idMarket {
            id
          }
      }
    }
  }
 }
}
`

export default class ModalView extends Component {


    state = {
    }

    componentDidMount() {
        
    }
    render() {
        return (
        <div>
            { this.props.values.ready
        && (<QueryRenderer
                environment={environment}
                query={MyQuery}
                variables={{
                    first: 3, 
                    baseMaterial: this.props.values.base_material, 
                    colour: this.props.values.color, 
                    waterProof: this.props.values.water_proof
                }}
                render={({error, props}) => {
              if (error) {
                return <div>{error.message}</div>
              } else if (props) {
                  console.log(props)
                return (
                  <div>
                      <Modal show={ this.props.values.ready }>
                          <Modal.Header>
                              ...
                          </Modal.Header>
                          <Modal.Body style={{
                              display: 'flex',
                              flexDirection: 'row',
                              flex: 1
                          }}>
                                { props.wallpapers.edges.map(({node}) => 
                                    (<div style={{width: '50%', textAlign: 'center'}}>
                                    <img src={ `../../slides/${node.picturesSet.set}` } styles={{width: '100%'}} alt=""/>
                                    </div>))}
                          </Modal.Body>
                          <Modal.Footer>
                              ...
                          </Modal.Footer>
                      </Modal>
                    </div>
                )
              }
              return <div>Loading</div>
            }}
          />)}

          {/* { this.props.values.ready 
            && (<Button onClick={ ()=> this.setState({show: true}) }>Ok</Button> )
          } */}
        </div>
        )
    }
}
