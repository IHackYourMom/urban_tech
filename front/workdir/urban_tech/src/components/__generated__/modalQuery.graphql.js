/**
 * @flow
 * @relayHash f4a1af478ca2b281bbd7e927c283030b
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type modalQueryVariables = {|
  first: number,
  baseMaterial: string,
  colour: string,
  waterProof: string,
|};
export type modalQueryResponse = {|
  +wallpapers: ?{|
    +edges: $ReadOnlyArray<?{|
      +node: ?{|
        +baseMaterial: ?string,
        +colour: ?string,
        +waterProof: ?string,
        +picturesSet: ?$ReadOnlyArray<?{|
          +image: string,
          +idMarket: {|
            +id: string
          |},
        |}>,
      |}
    |}>
  |}
|};
export type modalQuery = {|
  variables: modalQueryVariables,
  response: modalQueryResponse,
|};
*/


/*
query modalQuery(
  $first: Int!
  $baseMaterial: String!
  $colour: String!
  $waterProof: String!
) {
  wallpapers(first: $first, baseMaterial: $baseMaterial, colour: $colour, waterProof: $waterProof) {
    edges {
      node {
        baseMaterial
        colour
        waterProof
        picturesSet {
          image
          idMarket {
            id
          }
          id
        }
        id
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "first",
    "type": "Int!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "baseMaterial",
    "type": "String!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "colour",
    "type": "String!",
    "defaultValue": null
  },
  {
    "kind": "LocalArgument",
    "name": "waterProof",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "baseMaterial",
    "variableName": "baseMaterial",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "colour",
    "variableName": "colour",
    "type": "String"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first",
    "type": "Int"
  },
  {
    "kind": "Variable",
    "name": "waterProof",
    "variableName": "waterProof",
    "type": "String"
  }
],
v2 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "baseMaterial",
  "args": null,
  "storageKey": null
},
v3 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "colour",
  "args": null,
  "storageKey": null
},
v4 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "waterProof",
  "args": null,
  "storageKey": null
},
v5 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "image",
  "args": null,
  "storageKey": null
},
v6 = {
  "kind": "ScalarField",
  "alias": null,
  "name": "id",
  "args": null,
  "storageKey": null
},
v7 = {
  "kind": "LinkedField",
  "alias": null,
  "name": "idMarket",
  "storageKey": null,
  "args": null,
  "concreteType": "Wallpaper",
  "plural": false,
  "selections": [
    v6
  ]
};
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "modalQuery",
  "id": null,
  "text": "query modalQuery(\n  $first: Int!\n  $baseMaterial: String!\n  $colour: String!\n  $waterProof: String!\n) {\n  wallpapers(first: $first, baseMaterial: $baseMaterial, colour: $colour, waterProof: $waterProof) {\n    edges {\n      node {\n        baseMaterial\n        colour\n        waterProof\n        picturesSet {\n          image\n          idMarket {\n            id\n          }\n          id\n        }\n        id\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "modalQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "wallpapers",
        "storageKey": null,
        "args": v1,
        "concreteType": "WallNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "WallNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "WallNode",
                "plural": false,
                "selections": [
                  v2,
                  v3,
                  v4,
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "picturesSet",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Pictures",
                    "plural": true,
                    "selections": [
                      v5,
                      v7
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "modalQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "wallpapers",
        "storageKey": null,
        "args": v1,
        "concreteType": "WallNodeConnection",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "edges",
            "storageKey": null,
            "args": null,
            "concreteType": "WallNodeEdge",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "node",
                "storageKey": null,
                "args": null,
                "concreteType": "WallNode",
                "plural": false,
                "selections": [
                  v2,
                  v3,
                  v4,
                  {
                    "kind": "LinkedField",
                    "alias": null,
                    "name": "picturesSet",
                    "storageKey": null,
                    "args": null,
                    "concreteType": "Pictures",
                    "plural": true,
                    "selections": [
                      v5,
                      v7,
                      v6
                    ]
                  },
                  v6
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '6dbd7a60063046709231fe40bec93c28';
module.exports = node;
