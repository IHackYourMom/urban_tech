import React, { Component } from 'react';
import AssisMsg from './assisMsg'
import BuyerMsg from './buyerMsg'
import socketIOClient from 'socket.io-client'
import Artyom from 'artyom.js'
import ModalView from './modal'


export default class Assistant extends Component {
    
    componentDidMount() {
    }
    
    
    state = {
        msgs: [
            {
                text: `Бонжур, я Ваш персональный помощник Леруль, скажите, какие обои Вы бы хотели. `, 
                owner: 'bot'
            }
        ],
        text: '',
        values : {
            color: null,
            water_proof: null,
            base_material: null,
            ready: false
        }
    };
    
    constructor() {
        super();
        console.log('леха пидор')
        this.recognition = new window.webkitSpeechRecognition();
        this.recognition.lang = 'ru-RU';     
        this.recognition.onresult = (event)=> {
            var last = event.results.length - 1;
            console.log(event.results[last][0].transcript);
            this.send(event.results[last][0].transcript)
        }        
        this.recognition.onspeechend = function() {
            // this.recognition.stop();
        }        
        this.recognition.onnomatch = function(event) {
            console.log(event.results[0])
        }        
        this.recognition.onerror = function(event) {
            console.log("Error")
        }   
        this.artyom = new Artyom();
        this.artyom.initialize({
            lang:"ru-RU"
        })
    }

    ndsbdsj= () => {
        this.recognition.start()
    }
    
    send = (val) => {                 
        // const socket = socketIOClient('http://0.0.0.0:21092/');
        this.state.text = val
        this.state.msgs.push({text: val, owner: 'buyer'})
        this.textFetch()
        // this.setState({ msgs: this.state.msgs })
        // socket.emit('change text', {text: this.state.text, values: this.state.values} ) 
    }
    
    textFetch = async () => {
        let response = await fetch('http://0.0.0.0:21090/bot/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify({text: this.state.text, values: this.state.values})
        })
        response = await response.json()
        this.state.msgs.push({text: response.text, owner: 'bot'})
        this.setState({text: response.text, values: response.values})
        console.log(response)
        this.artyom.say(response.text ,{
            onStart:()=>{
                console.log("The text has been started.");
            },
            onEnd:()=>{
                this.recognition.start()
                console.log("The text has been finished.");
            }
        });
    } 

render() {
    const socket = socketIOClient('http://0.0.0.0:21092/')
    socket.on('change text', (col) => {
        console.log(col)
    })
    return (
        <div>
            <div id='ass' style={{
                display: 'none',
                position: 'absolute',
                bottom: 10,
                right: 10
            }}>
                { this.state.msgs.map(element => {
                    if(element.owner == 'bot'){
                        return <AssisMsg text={ element.text }/>
                    }
                    else {
                        return <BuyerMsg text={ element.text }/>
                    }
                })}
                <input type="text" id='inp'/>
                <button onClick={ this.ndsbdsj }>SEND</button>
                <p>{ this.state.text }</p>
            </div>
            <ModalView {...this.state}/>        
        </div>
        )
    }
}
